﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

using Backend.Models;
using Backend.Context;
using Backend.Repository;

namespace Backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            // string connection = "server=myverybestserver.mysql.database.azure.com;port=3306;database=bookstoredatabase;user=michael@myverybestserver;password=jdlaeRQ123!";
            
            // {
            //     _context.Books.Add(new Book 
            //     {
            //         Id = 123,
            //         PictureId = 12345,
            //         Title = "test",
            //         Author = "test",
            //         Genre = "genre",
            //         Price = 1
            //     });
            //     _context.SaveChanges();
            //     var books = _context.Books.ToList();
            //     foreach (var b in books)
            //     {
            //         Console.WriteLine("Ok -- {0}", b.Title);
            //     }
            // }   
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
