using System;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore;

namespace Backend.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int Amount { get; set; }
    }
}