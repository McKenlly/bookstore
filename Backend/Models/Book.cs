using System;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore;

namespace Backend.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string PictureId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public int Amount { get; set;}
        public double? Price { get; set; }
    }
}