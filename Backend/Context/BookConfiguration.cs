using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MySql.Data.EntityFrameworkCore;

using Backend.Models;
namespace Backend.Context
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
        {
            public void Configure(EntityTypeBuilder<Book> builder)
            {
                string primaryKey = "pk_book";
                builder.HasKey(b => b.Id).HasName(primaryKey);
                builder.HasIndex(b => b.Title).IsUnique();
                builder.Property(b => b.Title).IsRequired();
                builder.Property(b => b.Author).IsRequired();
                builder.Property(b => b.Genre).IsRequired();
                builder.Property(b => b.PictureId).HasColumnType("tinytext");
                builder.Property(b => b.Title).HasColumnType("tinytext");
                builder.Property(b => b.Author).HasColumnType("tinytext");
                builder.Property(b => b.Genre).HasColumnType("tinytext");
            }
        }
}