using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Backend.Models;
using Backend.Repository;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookStoreController : ControllerBase
    {
        private readonly IBookStoreRepositoryAsync _repository;
        
        public BookStoreController(IBookStoreRepositoryAsync repository)
        {
            _repository = repository;
        }
        
        /// <summary>
        /// Get all books from Book Store.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// https://localhost:5001/api/BookStore
        /// </remarks>
        // GET api/bookstore
        [HttpGet]
        public async Task<IEnumerable<Book>> Get()
        {
            return await _repository.GetAllBooksAsync();
        }

        /// <summary>
        /// Get a book from Book Store using BookId.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// https://localhost:5001/api/BookStore/test
        /// </remarks>
        // GET api/bookstore/5
        [HttpGet("{genre}")]
        public async Task<IEnumerable<Book>> GetByGenre(string genre)
        {
            return await _repository.GetAllBooksByGenreAsync(genre);
        }

        /// <summary>
        /// Buy a book from Book Store using Transaction.
        /// </summary>
        /// <remarks>
        /// Sample json:
        ///
        ///     {
        ///         "id": 1,
        ///         "amount": 0
        ///     }
        /// </remarks>
        // POST api/bookstore
        [HttpPost]
        public async Task<bool> Post([FromBody] Transaction transaction)
        {
            return await _repository.BuyBookAsync(transaction);
        }
    }
}
