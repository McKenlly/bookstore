using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Backend.Models;
using Backend.Repository;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DebugController : ControllerBase
    {
        private readonly IBookStoreRepositoryAsync _repository;
        public DebugController(IBookStoreRepositoryAsync repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Use only for test.
        /// </summary>
        // GET api/debug
        [HttpGet]
        public async Task<IEnumerable<Book>> InitAndTest()
        {

            Book[] books = new Book[]
            {
                new Book 
                {
                    // Id = 0,
                    PictureId = "http://test.com",
                    Title = "War and Peace",
                    Author = "L.Tolstoy",
                    Genre = "Novel",
                    Amount = 10,
                    Price = 100
                },
                new Book 
                {
                    // Id = 0,
                    PictureId = "http://test.com",
                    Title = "Eugene Onegin",
                    Author = "A. Pushkin",
                    Genre = "Novel",
                    Amount = 100,
                    Price = 10
                },
                new Book 
                {
                    // Id = 0,
                    PictureId = "http://test.com",
                    Title = "Fathers and Sons",
                    Author = "I. Turgenev",
                    Genre = "Romance Novel",
                    Amount = 1,
                    Price = 10
                }
            };
            await _repository.AddBookRangeAsync(books);
            return await _repository.GetAllBooksAsync();
        }
    }
}
