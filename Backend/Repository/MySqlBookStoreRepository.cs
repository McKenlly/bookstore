using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

using Backend.Context;
using Backend.Models;

namespace Backend.Repository
{
    public class MySqlBookStoreRepository : IDisposable,  IBookStoreRepositoryAsync
    {
        private readonly BookStoreContext _context = null;
        public MySqlBookStoreRepository(BookStoreContext context) 
        {
            _context = context;
        }
        public async Task<IEnumerable<Book>> GetAllBooksAsync()
        {
            return await _context.Books.ToListAsync();
        }
        public async Task<IEnumerable<Book>> GetAllBooksByGenreAsync(string genre)
        {
            return await (from book in _context.Books
                          where book.Genre == genre 
                          select book ).ToListAsync();
        }
        public async Task<bool> BuyBookAsync(Transaction transaction)
        {
            Book book = await _context.Books.FirstOrDefaultAsync(b => b.Id == transaction.Id);
            if (book != null)
            {
                if (book.Amount - transaction.Amount > 0)
                {
                    book.Amount -= transaction.Amount;
                    if (book.Amount == 0)
                        _context.Books.Remove(book);
                    else 
                        _context.Books.Update(book);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            return false;       
        }
        public async Task<int> AddBookAsync(Book book)
        {
            _context.Books.Add(book);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> AddBookRangeAsync(Book[] book)
        {
            _context.Books.AddRange(book);
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {

        }
    }
}