using System.Collections.Generic;
using System.Threading.Tasks;

using Backend.Models;

namespace Backend.Repository
{
    public interface IBookStoreRepositoryAsync
    {
        Task<IEnumerable<Book>> GetAllBooksAsync();
        Task<IEnumerable<Book>> GetAllBooksByGenreAsync(string genre);
        Task<bool> BuyBookAsync(Transaction transaction);
        Task<int> AddBookAsync(Book book);
        Task<int> AddBookRangeAsync(Book[] book);
        // Task<bool> RemoveBookAsync(int bookId);
        // Task<bool> BuyBookAsync(int bookId);
    //     Task<bool> UpdateBookAsync(Book book);
    }
}